<?php

return[
    'access' =>[
        'list_category' => 'list_category',
        'add_category' => 'add_category',
        'edit_category' => 'edit_category',
        'delete_category' => 'delete_category'
    ]
];
