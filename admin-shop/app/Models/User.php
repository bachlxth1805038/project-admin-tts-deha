<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'ruler',
        'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function getWithPaginate()
    {
        return $this->paginate(5);
    }

    public function hasRoles($roles = []): bool
    {
        return $this->roles()->whereIn('name', $roles)->count() > 0;
    }

    public function search($data)
    {
        return $this->withName($data)->when($data, function ($query) use ($data) {
            return $query->orWhere('status', (int)$data);
        })->paginate(5);
    }

    public function checkPermissionAccess($permissionCheck)
    {
        $roles = auth()->user()->roles;
        foreach ($roles as $role) {
            $permissions = $role->permissions;
            if ($permissions->contains('key_code', $permissionCheck)) {
                return true;
            }
        }
        return false;
    }

    public function scopeWithName($query, $name)
    {
        return $name
            ? $query->where('name', 'LIKE', '%' . $name . '%')
            : null;
    }
}
