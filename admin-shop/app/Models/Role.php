<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';
    protected $guarded = [];


    protected $fillable = [
        'name',
        'description',
    ];

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }

    public function getWithPaginate()
    {
        return Role::paginate(5);
    }

    public function search($data)
    {
        return Role::withName($data)->when($data, function ($query) use ($data) {
            return $query;
        })->paginate(5);
    }

    public function scopeWithName($query, $name)
    {
        return $name
            ? $query->where('name', 'LIKE', '%' . $name . '%')
            : null;
    }

}
