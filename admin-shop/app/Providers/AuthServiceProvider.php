<?php

namespace App\Providers;

use App\Models\Category;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use App\Policies\CategoryPolicy;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
//         'App\Models' => 'App\Policies\ModelPolicy',

    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('category-list', function ($user) {
            return $user->checkPermissionAccess(config('permissions.access.list_category'));
        });
        Gate::define('category-add', function ($user) {
            return $user->checkPermissionAccess(config('permissions.access.add_category'));
        });
        Gate::define('category-edit', function ($user) {
            return $user->checkPermissionAccess(config('permissions.access.edit_category'));
        });
        Gate::define('category-delete', function ($user) {
            return $user->checkPermissionAccess(config('permissions.access.delete_category'));
        });
    }
}
