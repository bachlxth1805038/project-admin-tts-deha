<?php

namespace App\Traits;

trait HasImage
{
    public function isVerify($request): bool
    {
        if ($request->hasFile('thumbnail')) {
            return true;
        }
        return false;
    }

    public function createImage($request): string
    {
        $fileName = '';
        if ($this->isVerify($request)) {
            $file = $request->thumbnail;
            $fileName = time() . str_replace(' ', '_', $file->getClientOriginalName());
            $file->move(IMAGE_PATH, $fileName);
        }
        return $fileName;
    }

    public function deleteImage($path)
    {
        $filePath = IMAGE_PATH . $path;
        if (file_exists($filePath)) {
            unlink($filePath);
        }
    }

    public function updateImage($request, $currentImage)
    {
        $fileName = $currentImage;
        if ($this->isVerify($request)) {
            if ($currentImage) {
                $this->deleteImage($currentImage);
            }
            $fileName = $this->createImage($request);
        }
        return $fileName;
    }
}
