<?php

namespace App\Http\Requests;

use App\Rules\CategoryRule;
use Illuminate\Foundation\Http\FormRequest;

class StoreUserRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => [
                'required',
                'min:2',
                'max:255',
                'unique:users,name',
            ],
            'email' => [
                'required',
                'min:2',
                'max:255',
                'unique:users,email',
            ]
        ];
    }

    public function messages()
    {
        return [
            'required' => ':attribute ko được để trống ',
            'min' => ':attribute phải đủ từ 2-255 kí tự',
            'max' => ':attribute phải đủ từ 2-255 kí tự',
            'unique' => ':attribute đã tồn tại',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Tên người dùng',
            'email' => 'Email người dùng',
        ];
    }
}
