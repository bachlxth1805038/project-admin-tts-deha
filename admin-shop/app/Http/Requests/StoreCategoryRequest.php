<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\CategoryRule;

class StoreCategoryRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => [
                'required',
                'min:2',
                'max:255',
                new CategoryRule(),
            ],
        ];
    }

    public function messages()
    {
        return [
            'required' => ':attribute ko được để trống ',
            'min' => ':attribute phải đủ từ 2-255 kí tự',
            'max' => ':attribute phải đủ từ 2-255 kí tự',
            'unique' => ':attribute đã tồn tại',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Tên danh mục sản phẩm',
        ];
    }
}
