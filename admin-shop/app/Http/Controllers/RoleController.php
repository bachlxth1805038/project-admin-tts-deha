<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreRoleRequest;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    public $user;
    public $role;
    public $permission;

    public function __construct(Role $role, User $user, Permission $permission)
    {
        $this->user = $user;
        $this->permission = $permission;
        $this->role = $role;
    }

    public function index()
    {
        $roles = $this->role->getWithPaginate();
        return view('admin.pages.role.index', compact('roles'));
    }

    public function list(Request $request)
    {
        $data = $request['data'];
        $roles = $this->role->search($data);
        return view('admin.pages.role.list', compact('roles'));
    }

    public function create()
    {
        $permissionsParent = $this->permission->where('parent_id', 0)->get();
        $roles = $this->role->all();
        return view('admin.pages.role.add', compact('roles', 'permissionsParent'));
    }

    public function store(StoreRoleRequest $request)
    {
        $role = $this->role->create([
            'name' => $request->name,
            'description' => $request->description,
        ]);
        $role->permissions()->sync($request->permission_id);
        return redirect()->route('role.index');
    }

    public function update(StoreRoleRequest $request, $id)
    {
        $role = $this->role->findOrFail($id);
        $data = $request->all();
        ($role->update($data));
        $role->permissions()->sync($request->permission_id);
        return redirect()->route('role.index');
    }

    public function destroy($id)
    {
        $role = $this->role->findOrFail($id);
        $role->destroy();
        return response()->json(['message' => 'Successfully Deleted']);
    }

    public function search(Request $request)
    {
        $data = $request['data'];
        $roles = $this->role->search($data);
        return view('admin.pages.role.list', compact('roles'));
    }

    public function edit($id)
    {
        $permissionsParent = $this->permission->where('parent_id', 0)->get();
        $role = $this->role->findOrFail($id);
        $permissionsChecked = $role->permissions;
        return view('admin.pages.role.edit', compact('role', 'permissionsParent', 'permissionsChecked'));
    }
}
