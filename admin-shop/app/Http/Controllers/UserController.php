<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public $user;
    public $role;

    public function __construct(User $user, Role $role)
    {
        $this->user = $user;
        $this->role = $role;
    }

    public function index()
    {
        $roles = $this->role->all();
        $users = $this->user->getWithPaginate();
        return view('admin.pages.user.index', compact('users', 'roles'));
    }

    public function list(Request $request)
    {
        $data = $request['data'];
        $users = $this->user->search($data);
        return view('admin.pages.user.list', compact('users'));
    }

    public function create()
    {
        $roles = $this->role->all();
        return view('admin.pages.user.add', compact('roles'));
    }

    public function store(StoreUserRequest $request)
    {
        $user = $this->user->create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->newPassword),
        ]);
        $user->roles()->sync($request->role_id);
        return redirect()->route('user.index');
    }

    public function edit($id)
    {
        $user = $this->user->with('roles')->findOrFail($id);
        $roles = $this->role->all();
        return view('admin.pages.user.edit', compact('user', 'roles'));
    }

    public function update(UpdateUserRequest $request, $id)
    {
        $user = $this->user->findOrFail($id);
        $user->update($request->all());
        $user->roles()->sync($request->role_id);
        return response()->json(['message' => 'Successfully Edited']);
    }

    public function destroy($id)
    {
        $user = $this->user->findOrFail($id);
        $user->destroy();
        return response()->json(['message' => 'Successfully Deleted']);
    }

    public function search(Request $request)
    {
        $data = $request['data'];
        $users = $this->user->search($data);
        return view('admin.pages.user.list', compact('users'));
    }
}
