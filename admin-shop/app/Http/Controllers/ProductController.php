<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    protected $product;
    protected $category;

    public function __construct(Product $product, Category $category)
    {
        $this->product = $product;
        $this->category = $category;
    }

    public function index()
    {
        $categories = $this->category->all();
        return view('admin.pages.product.index', compact('categories'));
    }

    public function list(Request $request)
    {
        $data = $request['data'];
        $products = $this->product->search($data);
        return view('admin.pages.product.list', compact('products'));
    }

    public function create()
    {
        $category = $this->category->getWithPagination();
        return view('admin.pages.product.add', compact('category'));
    }

    public function store(StoreProductRequest $request)
    {
        $this->product->store($request);
        return redirect()->route('product.index')->with('message', 'Successfully Created');
    }

    public function edit($id)
    {
        $category = $this->category->getWithPagination();
        $product = $this->product->findOrFail($id);
        return response()->json(['category' => $category, 'product' => $product]);
    }

    public function update(UpdateProductRequest $request, $id)
    {
        $data = $request->all();
        $product = $this->product->findOrFail($id);
        $fileName = $this->product->updateImage($request, $product->thumbnail);
        $data['thumbnail'] = $fileName;
        $product->update($data);
        return response()->json(['message' => "Successfully Edited"]);
    }

    public function destroy($id)
    {
//        $product = $this->product->findOrFail($id);
//        $product->destroy();
        Product::destroy($id);
        return response()->json(['message' => "Successfully deleted"]);
    }

    public function search(Request $request)
    {
        $data = $request['data'];
        $products = $this->product->search($data);
        return view('admin.pages.product.list', compact('categories', 'products'));
    }
}
