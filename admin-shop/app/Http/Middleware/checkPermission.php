<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use PhpParser\Node\Expr\ClosureUse;

class checkPermission
{
    public function handle($request, Closure $next, $permission = null)
    {
        if (Auth::user()->checkPermissionAccess($permission)) {
            return $next($request);
        }
        return redirect(403, 'Unauthorized action.');
    }
}
