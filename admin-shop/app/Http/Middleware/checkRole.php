<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class checkRole
{
    public function handle($request, Closure $next)
    {
        $params = func_get_args();
        $roles = array_slice($params, 2);
        if (Auth::user()->hasRoles($roles)) {
            return $next($request);
        }
        abort(401);
    }
}
