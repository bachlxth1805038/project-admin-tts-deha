$(document).ready(function () {
    let urlUpdate;
    getList();

    // get list product
    function getList() {
        let url = $('#listProduct').data('action')
        callApi(GET_METHOD, null, url)
            .then((rs) => {
                $('#listProduct').replaceWith(rs);
            })
            .catch(function (error) {
            })
    }

    //show image
    function showImage(src, target) {
        let fr = new FileReader();
        fr.onload = function (e) {
            target.src = this.result;
        };
        src.addEventListener("change", function () {
            fr.readAsDataURL(src.files[0]);
        });
    }

    let src = document.getElementById("src");
    let target = document.getElementById("target");
    showImage(src, target);

    // edit product
    let productsIdUpdate;
    $(document).on('click', '.editProduct', function () {
        productsIdUpdate = $(this).data('id');
        $(`.errors`).html('')
        urlUpdate = $(this).data('action');
        $.ajax({
            url: 'admin/product/' + productsIdUpdate + '/edit',
            dataType: 'json',
            type: 'get',
            success: function (data) {
                $('.name').val(data.product.name);
                $('.quantity').val(data.product.quantity);
                $('.price').val(data.product.price);
                $('.imageThum').attr('src', 'img/upload/product/' + data.product.thumbnail);
                $('.description').val(data.product.description);
                $('.cateProduct').val(data.product.category_id);
                if (data.product.status == 1) {
                    $('.ht').attr('selected', 'selected');
                } else {
                    $('.kht').attr('selected', 'selected');
                }
            }
        });
    })

    $(document).on('click', '.btn-update-product', function (e) {
        e.preventDefault();
        $(".errorName").html('');

        $.ajax({
            url: 'admin/product/' + productsIdUpdate,
            data: new FormData($('#updateProductForm')[0]),

            contentType: false,
            processData: false,
            cache: false,
            type: 'post',

        }).then(res => {
            toastr.success(res.message, 'Notifications ', {timeOut: 3000});
            $('#edit').modal('hide');

        }).catch((res) => {
            showError(res);
        });
    })

    function showError(res) {
        let errors = res?.responseJSON?.errors;
        for (let errorName in errors) {
            $(`.error-${errorName}`).html(errors[errorName][0]);
        }
    }
})
