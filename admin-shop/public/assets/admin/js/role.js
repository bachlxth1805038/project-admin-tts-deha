$(document).ready(function () {
    let urlDelete;
    getList();

    function getList()
    {
        let url = $('#listRole').data('action')

        callApi(url, null, GET_METHOD)
            .then((rs) => {
                $('#listRole').replaceWith(rs);
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    $(document).on('click', '.editRole', function ()
    {
        $('.error').hide();
        let url = $(this).data('url');
        callApi(url, {}, GET_METHOD)
            .then((result) => {
                $('#role_name').val(result.data.name);
                $('#role_description').val(result.data.description);
            })
    });

    $(document).on('click', '.updateRole', function () {
        let data = $('#role_data').serialize();
        let urlUpdate = $(this).data('action');
        callApi(urlUpdate, data, PUT_METHOD)
            .then(() => {
                getList();
                $('#edit').modal('hide');
            }).catch((res) => {
            showError(res)
        });
    });

    function showError(res)
    {
        let errors = res?.responseJSON?.errors;
        for (let errorName in errors) {
            $(`.error-${errorName}`).html(errors[errorName][0]);
        }
    }

    $(document).on('click', '#btn-SearchRole', function ()
    {
        let data = $('#txt-SearchRole').val();
        let urlSearch = $(this).data('url');
        callApi(urlSearch, {data}, POST_METHOD)
            .then((result) => {
                $('#listRole').replaceWith(result);
            });
    });

    $(document).on('click', '.deleteRole', function ()
    {
        $('.error').hide();
        urlDelete = $(this).data('url');
    });

    $(document).on('click', '.deleteWithRole', function ()
    {
        callApi(urlDelete, {}, DELETE_METHOD).then((res) => {
            toastr.success(res.message, 'Thông báo', {timeOut: 3000});
            getList();
            $('#delete').modal('hide');
        })
    });
});
