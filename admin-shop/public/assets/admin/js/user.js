$(document).ready(function () {
    let urlDelete;
    let urlUpdate;
    getList();

    function getList() {
        let url = $('#listUser').data('action')
        callApi(url, null, GET_METHOD)
            .then((rs) => {
                $('#listUser').replaceWith(rs);
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    $(document).on('click', '.editUser', function () {
        $('.select2').select2();
        $('.error').hide();
        let url = $(this).data('url');

        callApi(url, {}, GET_METHOD)
            .then((result) => {
                $('body').find('#edit .modal-content').html(result);
            })
    });

    $(document).on('submit', '#user_data', function (e) {
        e.preventDefault();
        $(".errorName").html('');
        let data = $('#user_data').serialize();
        urlUpdate = $(this).data('action');
        callApi(urlUpdate, data, POST_METHOD)
            .then((res) => {
                toastr.success(res.message, 'Notifications', {timeOut: 3000});
                getList();
                $('#edit').modal('hide');
            }).catch((res) => {
            showError(res);
        });
    });

    function showError(res) {
        $('#errorMsg').removeClass('d-none');
        $.each(res.responseJSON.errors, function (key, value) {
            $('body').find('#errorMsg').html('<li>' + value + '</li>');
        })
    }

    $(document).on('click', '.deleteUser', function () {
        $('.error').hide();
        urlDelete = $(this).data('url');
    });

    $(document).on('click', '.deleteWithUser', function () {
        callApi(urlDelete, {}, DELETE_METHOD).then((res) => {
            toastr.success(res.message, 'Notifications', {timeOut: 3000});
            getList();
            $('#delete').modal('hide');
        })
    });

    $(document).on('click', '#btn-SearchUser', function () {
        let data = $('#txt-SearchUser').val();
        let urlSearch = $(this).data('url');
        callApi(urlSearch, {data}, POST_METHOD)
            .then((rs) => {
                $('#listUser').replaceWith(rs);
            });
    });

})
