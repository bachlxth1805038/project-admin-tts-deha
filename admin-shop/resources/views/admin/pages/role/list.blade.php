<div class="card-body list-multi" id="listRole" data-action="{{route('role.list')}}">
    <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
            <tr>
                <th>ID</th>
                <th>Role name</th>
                <th>Description</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody id="list-role">
            @foreach($roles as $key => $value)
                <tr>
                    <td>{{ $key+1 }}</td>
                    <td>{{ $value->name }}</td>
                    <td>{{ $value->description }}</td>
                    <td>
                        <a href="{{route('role.edit',$value->id)}}"><button class="btn btn-primary editRole" title="{{ "Edit ".$value->name }}"  data-target="#edit" type="button" data-id="{{ $value->id }}" data-url="{{route('role.getData',$value->id)}}" data-action="{{route('role.update',$value->id)}}"><i class="fas fa-edit"></i></button></a>
                        <button class="btn btn-danger deleteRole" data-url="{{route('role.destroy',$value->id)}}" title="{{ "Delete ".$value->name }}"  data-toggle="modal" data-target="#delete" type="button" data-id="{{ $value->id }}"><i class="fas fa-trash-alt"></i></button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
