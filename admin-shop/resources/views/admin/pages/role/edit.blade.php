@extends('admin.layouts.master')

@section('title')
    Edit Role
@endsection

@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Role</h6>
        </div>
        <div class="row" style="margin: 5px">
            <div class="col-lg-12">
                <form role="form" action="{{route('role.update',$role->id)}}" method="POST">
                    @method('PUT')
                    @csrf
                    <fieldset class="form-group">
                        <label>Role name</label>
                        <input class="form-control" name="name" placeholder="Enter role name" value="{{$role->name}}">
                        @if($errors->has('name'))
                            <div class="alert alert-danger">{{$errors->all()[0]}}</div>
                        @endif
                    </fieldset>
                    <div class="form-group">
                        <label>Description</label>
                        <input type="text" name="description" placeholder="Enter description role" class="form-control" value="{{$role->description}}">
                        @if($errors->has('description'))
                            <div class="alert alert-danger">{{$errors->all()[0]}}</div>
                        @endif
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            @foreach($permissionsParent as $permissionsParentItem)
                                <div class="card border-primary mb-3 col-md-12">
                                    <div class="card-header">
                                        <label>
                                            <input type="checkbox" value="" class="checkbox_wrapper">
                                        </label>
                                        Module {{$permissionsParentItem->name}}
                                    </div>
                                    <div class="row">
                                        @foreach($permissionsParentItem->permissionsChildrent as $permissionsChildrenItem)
                                            <div class="card-body text-primary col-md-3">
                                                <h5 class="card-title">
                                                    <label>
                                                        <input type="checkbox" name="permission_id[]"
                                                               {{$permissionsChecked->contains('id',$permissionsChildrenItem->id) ? 'checked' : '' }}
                                                               class="checkbox_childrent"
                                                               value="{{$permissionsChildrenItem->id}}">
                                                    </label>
                                                    {{$permissionsChildrenItem->name}}
                                                </h5>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <button type="submit" class="btn btn-success">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection


