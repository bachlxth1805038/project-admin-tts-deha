@extends('admin.layouts.master')

@section('title')
    Index Role
@endsection

@section('content')
    <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search formSearch" id="formSearch" >
        <div class="input-group" style="margin-top: 10px">
            <input type="text" id="txt-SearchRole" class="form-control bg-light border-0 abc" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2 ">
            <div class="input-group-append">
                <button class="btn btn-primary" type="button" id="btn-SearchRole" data-url="{{route('role.search')}}">
                    <i class="fas fa-search fa-sm"></i>
                </button>
                <a href="{{route('role.create')}}"><button type="button" class="btn btn-success">ADD</button></a>
            </div>
        </div>
    </form>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Role</h6>
        </div>
        <div id="listRole" data-action="{{route('role.list')}}">
        </div>
    </div>

    <!-- Edit Modal-->
    <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Chỉnh sửa Role <span class="title"></span></h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row" style="margin: 5px">
                        <div class="col-lg-12">
                            <form role="form" id="role_data">
                                <fieldset class="form-group">
                                    <label>Vai trò</label>
                                    <input class="form-control name" name="name" placeholder="Nhập tên vai trò " id="role_name">
                                    <span class="error" style="color: red; font-size: 1rem"></span>
                                    <span class="errors error-name  text-danger"></span>
                                </fieldset>

                                <fieldset class="form-group">
                                    <label>Mô tả vai trò</label>
                                    <input class="form-control name" name="description" placeholder="Nhập tên mô tả vai trò " id="role_description">
                                    <span class="error" style="color: red; font-size: 1rem"></span>
                                    <span class="errors error-name  text-danger"></span>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-success updateRole">Save</button>
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <!-- delete Modal-->
    <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Do you want to delete?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body" style="margin-left: 183px;">
                    <button type="button" class="btn btn-success deleteWithRole">Yes</button>
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">No</button>
                    <div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{asset('assets/admin/js/role.js')}}"></script>
@endsection

