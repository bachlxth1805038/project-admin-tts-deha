<div class="card-body list" id="listProduct" data-action="{{route('product.list')}}">
    <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Description</th>
                <th>Thumbnail</th>
                <th>Category</th>
                <th>Quantity</th>
                <th>Price</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>

            @foreach($products as $key => $value)
                <tr>
                    <td>{{ $loop->iteration}}</td>
                    <td>{{ $value->name }}</td>
                    <td>{{ $value->description }}</td>
                    <td>
                        <img src="{{asset('img/upload/product')}}{{ '/'.$value->thumbnail }}" width="100" height="100">
                    </td>
                    <td>{{ ($value->category)->name}}</td>
                    <td>{{ $value->quantity }}</td>
                    <td>{{ $value->price }}</td>
                    <td>
                        @if( $value->status == 1 )
                            {{"Active"}}
                        @else
                            {{"Deactive"}}
                        @endif
                        </td>

                    <td>
                        <button class="btn btn-primary editProduct"
                                title="{{ "Edit ".$value->name }}"
                                data-toggle="modal" data-target="#edit"
                                type="button"
                                data-url="{{route('product.getData',$value->id)}}"
                                data-action="{{route('product.update',$value->id)}}"
                                data-id="{{ $value->id }}">
                            <i class="fas fa-edit">

                            </i>
                        </button>
                        <button class="btn btn-danger deleteProduct" data-url="{{route('product.destroy',$value->id)}}" title="{{ "Delete ".$value->name }}" data-toggle="modal" data-target="#delete" type="button" data-id="{{ $value->id }}"><i class="fas fa-trash-alt"></i></button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="-pull-right">{{ $products->links() }}</div>
    </div>
</div>



