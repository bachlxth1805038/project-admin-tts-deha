@extends('admin.layouts.master')

@section('title')
    Index Product
@endsection

@section('content')
    <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search formSearch"
          id="formSearch">
        <div class="input-group" style="margin-top: 10px">
            <input type="text" id="txt-SearchProduct" class="form-control bg-light border-0 abc"
                   placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2 ">
            <div class="input-group-append">
                <button class="btn btn-primary" type="button" id="btn-SearchProduct"
                        data-url="{{route('product.search')}}">
                    <i class="fas fa-search fa-sm"></i>
                </button>
                <a href="{{route('product.create')}}">
                    <button type="button" class="btn btn-success">Add</button>
                </a>
            </div>
        </div>
    </form>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Index Product</h6>
        </div>
        <div id="listProduct" data-action="{{route('product.list')}}">
        </div>
    </div>

    <!-- Edit Modal-->
    <div class="modal fade " id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Product <span class="title"></span></h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger d-none" id="errorMsg" role="alert">
                        <ul></ul>
                    </div>
                    <div class="row" style="margin: 5px">
                        <div class="col-lg-12">
                            <form id="updateProductForm" method="post"
                                  enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <input type="hidden" name="id" class="idProduct">
                                <fieldset class="form-group">
                                    <label>Name Product</label>
                                    <input class="form-control name" name="name" placeholder="Enter name product "
                                           id="product_name">
                                    <div class=" errorName"></div>
                                    <span class="errors error-name  text-danger"></span>
                                </fieldset>
                                <div class="form-group">
                                    <label for="quantity">Quantity</label>
                                    <input type="number" name="quantity" min="1" value="1" class="form-control quantity"
                                           id="product_quantity">
                                    <div class=" errorQuantity"></div>
                                </div>
                                <div class="form-group">
                                    <label for="price">Price</label>
                                    <input type="text" name="price" placeholder="Enter Price" class="form-control price"
                                           id="product_price">
                                    <div class=" errorPrice"></div>
                                </div>
                                <img id="target" class="img img-thumbnail imageThum" width="100" height="100"
                                     lign="center">
                                <div class="form-group">
                                    <label>Thumbnail</label>
                                    <input type="file" name="thumbnail" class="form-control image" id="src">
                                    <span class="errors error-thumbnail  text-danger"></span>
                                    <div class=" errorImage"></div>
                                </div>
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea name="description" cols="5" rows="5"
                                              class="form-control description" id="product_description"></textarea>
                                    <div class=" errorDescription"></div>
                                </div>
                                <div class="form-group">
                                    <label>Category</label>
                                    <select class="form-control cateProduct" name="category_id" id="category_id">
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Status</label>
                                    <select class="form-control status" name="status" id="product_status">
                                        <option value="1" class="ht">Active</option>
                                        <option value="0" class="kht">Deactive</option>
                                    </select>
                                </div>
                                <button type="button" class="btn btn-success btn-update-product" value="Edit">Edit
                                </button>
                                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- delete Modal-->
    <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Do you want to delete ?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body" style="margin-left: 183px;">
                    <button type="button" class="btn btn-success deletePro">Yes</button>
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">No</button>
                    <div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{asset('assets/admin/js/product.js')}}"></script>
    <script src="{{asset('assets/admin/js/demo/edproduct.js')}}"></script>
@endsection

@section('style')
    <style>
        .errors {
            font-size: 1rem !important;
        }
    </style>
@endsection
