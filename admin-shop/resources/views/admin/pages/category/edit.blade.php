@extends('admin.layouts.master')

@section('title')
    Edit Category
@endsection

@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Category</h6>
        </div>
        <div class="row" style="margin: 5px">
            <div class="col-lg-12">
                <form role="form" action="{{route('category.update')}}" method="put">
                    @csrf
                    <fieldset class="form-group">
                        <label>Name</label>
                        <input class="form-control" name="name" value="{{$category->name}}">
                        <span class="error" style="color: red; font-size: 1rem"></span>
                    </fieldset>

                    <div class="form-group">
                        <label>Status</label>
                        <select class="form-control" name="status" >
                            <option value="1" >Active</option>
                            <option value="0" >{{$category->status}}</option>
                            <option value="0">Deactive</option>
                        </select>
                    </div>

                    <button type="submit" class="btn btn-success">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection


