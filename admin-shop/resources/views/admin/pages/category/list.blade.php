<div id="listCategory" data-action="{{route('category.list')}}">

<table class="table table-bordered" id="search_table">
           <thead>
           <tr>
               <th scope="col">ID</th>
               <th scope="col">Name</th>
               <th scope="col">Status</th>
               <th width="280px">Action</th>
           </tr>
           </thead>
           @foreach ($categories as $key => $category)
               <tbody id="myTable">
               <tr id="tr_{{ $category->id }}">
                   <td scope="col">
                       {{ $category->id }}
                   </td>
                   <td scope="col">
                       {{ $category->name }}
                   </td>
                   <td scope="col">
                       @if($category->status == 1)
                           {{"Active"}}
                       @else
                           {{"Deactive"}}
                       @endif
                   </td>
                   <td>
                       <a class="btn btn-primary editCategory" title="{{"Edit".$category->name}}"
                          data-toggle="modal" data-target="#edit"
                          type="button" data-id="{{ $category->id }}"
                          data-url="{{route('category.show', $category->id)}}"
                          data-action="{{route('category.update', $category->id)}}">
                           <i class="fas fa-edit"></i>
                       </a>

                       <button class="mr-2 btn btn-danger btn-delete deleteCategory"
                               data-url="{{route('category.destroy', $category->id)}}"
                               title="{{"Delete".$category->name}}" data-toggle="modal"
                               data-target="#delete" type="button" data-id="{{$category->id}}">
                           <i class="fas fa-trash"></i>
                       </button>
                   </td>
               </tr>
               </tbody>
           @endforeach
    </table>
    <div class="-pull-right">{{$categories->links()}}</div>
</div>
