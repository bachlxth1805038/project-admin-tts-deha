<div class="modal-header">
    <h5 class="modal-title" id="exampleModalLabel">Edit User <span class="title"></span></h5>
    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div>
<div class="modal-body">
    <div class="alert alert-danger d-none" id="errorMsg" role="alert">
        <ul></ul>
    </div>
    <div class="row" style="margin: 5px">
        <div class="col-lg-12">
            <form role="form" id="user_data" data-action="{{ route('user.update', $user->id) }}" method="post">
                @method('PUT')
                <fieldset class="form-group">
                    <label>Name</label>
                    <input class="form-control " name="name" value="{{ $user->name }}" placeholder="Enter name User "
                           id="user_name">
                    <div class=" errorName"></div>
                    <span class="errors error-name  text-danger"></span>
                </fieldset>

                <fieldset class="form-group">
                    <label>Email</label>
                    <input class="form-control" name="email" value="{{ $user->email }}" placeholder="Enter name Email "
                           id="user_email">
                    <div class=" errorName"></div>
                    <span class="errors error-name text-danger"></span>
                </fieldset>
                <fieldset class="form-group">
                    <label>Role</label>
                    <select class="select2 form-control" multiple="multiple" name="role_id[]"
                            data-placeholder="Select a role">
                        @foreach($roles as $role)
                            <option
                                {{ $user->roles->contains('id', $role->id) ? 'selected' : '' }}
                                value="{{ $role->id }}">{{ $role->name }}
                            </option>
                        @endforeach
                    </select>
                </fieldset>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success updateUser">Save</button>
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>


