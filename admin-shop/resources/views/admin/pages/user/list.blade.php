<div class="card-body list-multi" id="listUser" data-action="{{route('user.list')}}">
    <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody id="list-user">
            @foreach($users as $key => $value)
                <tr>
                    <td>{{ $key+1 }}</td>
                    <td>{{ $value->name }}</td>
                    <td>{{ $value->email }}</td>
                    <td>
                        <button class="btn btn-primary editUser" title="{{ "Sửa ".$value->name }}" data-toggle="modal" data-target="#edit" type="button" data-id="{{ $value->id }}" data-url="{{route('user.edit',$value->id)}}" data-action="{{route('user.update',$value->id)}}"><i class="fas fa-edit"></i></button>
                        <button class="btn btn-danger deleteUser" data-url="{{route('user.destroy',$value->id)}}" title="{{ "Xóa ".$value->name }}" data-toggle="modal" data-target="#delete" type="button" data-id="{{ $value->id }}"><i class="fas fa-trash-alt"></i></button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{--        <div class="pull-right">{{ $category->links() }}</div>--}}
    </div>
</div>
