@extends('admin.layouts.master')

@section('title')
    Index User
@endsection

@section('content')
    <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search formSearch" id="formSearch" >
        <div class="input-group" style="margin-top: 10px">
            <input type="text" id="txt-SearchUser" class="form-control bg-light border-0 abc" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2 ">
            <div class="input-group-append">
                <button class="btn btn-primary" type="button" id="btn-SearchUser" data-url="{{route('user.search')}}">
                    <i class="fas fa-search fa-sm"></i>
                </button>
                <a href="{{route('user.create')}}"><button type="button" class="btn btn-success">ADD</button></a>
            </div>
        </div>
    </form>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">User</h6>
        </div>
        <div id="listUser" data-action="{{route('user.list')}}">
        </div>
    </div>

    <!-- Edit Modal-->
    <div class="modal fade " id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

            </div>
        </div>
    </div>

    <!-- delete Modal-->
    <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Do you want to delete?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body" style="margin-left: 183px;">
                    <button type="button" class="btn btn-success deleteWithUser">yes</button>
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">no</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{asset('assets/admin/js/user.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
@endsection

